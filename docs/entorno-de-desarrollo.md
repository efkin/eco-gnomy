# How to configure a development enviroment (Debian stable)

## Prepare the system

* Create a dedicated user

``` sudo adduser ecognome ```

* Install git

``` sudo apt-get install git ```

* Install python3 and pip3 

``` sudo apt-get install python3 python3-pip python3-dev ```

* Upgrade pip3

``` sudo pip3 install --upgrade pip ```

* Log in as the dedicated user

``` sudo su - ecognome ```

* Generate an ssh key and add it to your gitlab configuration

```
#the public key will be stored in .ssh/id_rsa.pub

ssh-keygen

```

* Fork the main repo via the web interface and clone the fork in your local enviroment

``` git clone git@gitlab.com:my-gitlab-user/eco-gnomy.git ```

* Create a dedicated virtual enviroment

``` pyvenv eco-env ```

* Update `.bashrc` to automatically activate the virtualenv when login as `ecognome` user

``` echo "source eco-env/bin/activate" >> .bashrc ```

* Activate the virtual enviroment

``` source eco-env/bin/activate ```

## Prepare the django project

* Install the project dependencies

```
cd eco-gnomy

pip3 install --upgrade -r requirements/development

```

* Apply the migrations

```
cd ecognome

python3 manage.py migrate
```

* Create a superuser for admin related stuff

``` python3 manage.py createsuperuser ```

* Run the dev server

``` python3 manage.py runserver ```


