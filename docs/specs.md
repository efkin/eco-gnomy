# Especificaciones eco-gnome

## Resumen workflow

### Metálico
El GT Economía se reune. Entre mas de una persona, van rellenando los movimientos de metálico.

Estos pueden ser referentes a entradas como:

* la cuota de participación en la cooperativa de viviendas 
* un importe en concepto de alquiler de una habitación/piso [0]
* un importe en concepto de alquiler de un espacio para un proyecto productivo
* un importe en concepto de beneficio por proyecto productivo
* un importe en concepto de alquiler de un trastero
* la cuota de mantenimiento mensual
* donativo ( ya sea de parte de un habitante, que de un proyecto productivo, que de un visitante)
* beneficios de un evento
* otros importes asociados a una nota descriptiva.

O bien pueden ser referentes a salidas como:

* la cuota de alquiler con opción a compra destinada al Ollé
* Material de rehabilitación
* Limpieza
* Bombonas de butano
* Bancos (transferencia de metalico a banco) [1] 
* Transporte
* Otros importes asociados a una nota descriptiva

### Bancos
En un orden mas o menos indiferente proceden a revisar Fiare, Triodos y Paypal.
En Paypal tendrán que rellenar entradas y salidas. [2] 
En Fiare deberían incluirse entre las entradas importes como:

* transferencias entre bancos.
* además de las especificadas para metálico.

y en las salidas:

* Impuestos
* Comisiones
* Transferencias entre bancos
* La cuota de internet
* El agua
* El importe del pago del crédito de fiare
* Otros importes acompañados de una nota descriptiva.

En el caso de Triodos, para las entradas se mantendrán los mismos parámetros que para metálico. [3]
Para las salidas deberían incluirse importes como:

* Impuestos
* Comisiones
* La cuota de compraventa del Ollé
* Transferencia entre bancos
* Internet
* Agua
* Otros importes acompañados de una nota descriptiva.

Una vez terminado el proceso de inserción de datos, la aplicación debería automaticamente actualizar/generar las siguientes planillas:

* Eventos [módulo a parte]
* Mantenimiento [habitante, visita, invitadx, otros(dia abierto)]
* Proyectos
* Pisos
* Histórico de pisos

Además debería de generar balances por meses, trimestres, semestres y año.

Por otro lado todas las habitantes podrán consultar la situación de cada piso.

Estaría bien que fuese capaz de generar previsiones y gráficos relativos a las previsiones.

## Preguntas
[0] se puede alquilar un piso entero? si

[1] en que consiste esta columna? explicado

[2] podriamos considerar que en ambas direcciones es un movimiento de donativos? Si

[3] añadimos transferencias entre bancos en las entradas? Si