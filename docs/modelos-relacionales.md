Modelos relacionales
====================

![Modelos relacionales](img/modelos_relacionales.png)


Cuota Mantenimiento
-------------------

### Descripción
El modelo que recoge la información sobre una cuota de mantenimiento.

### Atributos
* importe

* tipo

Cuota Piso
----------

### Descripción
El modelo que recoge la información sobre una cuota de adquisición del derecho de uso permanente de una vivienda.

### Atributos
* importe

* descripción

Piso
----

### Descripción
El modelo que recoge la información sobre los diferentes pisos.

### Atributos
* nombre del piso

* total pagado del piso

* valor total del piso

* deuda piso

* Cuota Piso (Foreign Key)

Trastero
--------

### Descripción
El modelo que recoge la información sobre un trastero.

### Atributos
* nombre del trastero

* deuda del trastero

* metros cuadrados / cuota

Proyecto Productivo
-------------------

### Descripción
El modelo que recoge la información sobre un proyecto productivo.

### Atributos
* nombre del proyecto

* metros cuadrados / cuota

* deuda del proyecto productivo

* fecha de creación

* fecha de disolución

Persona
-------

### Descripción
Clase genérica para habitante, invitado y huesped.

### Atributos
* nombre
* deuda mantenimiento
* Cuota Mantenimiento (Foreign Key)
* fecha de entrada
* fecha de salida
* donativo

Habitante
---------

### Hereda de
```Persona```

### Descripción
El modelo que recoge la información sobre una habitante.
Una habitante es aquella persona que reside permanentemente en el territorio.
Ya sea en viviendas, casa roja o el camping.
Tendrá una cuota de mantenimiento de 10€ al mes, con posibilidad de modificarla.

### Atributos
* Heredados de ```Persona```

Invitado
--------

### Hereda de
```Persona```

### Descripción
El modelo que recoge la información sobre una invitada.
Una invitada es aquella persona que reside temporalmente en el territorio.
Aunque su estancia es mínimo de un mes.
Tendrá una cuota de mantenimiento de 15€ al mes, con posiblidad de modificarla.
Una invitada puede convertirse en habitante.
Para esto se cierra la estancia como invitada y se crea una habitante nueva.

### Atributos
* Heredados de ```Persona```

Huesped
-------

### Hereda de
```Persona```

### Descripción
El modelo que recoge la información sobre una huesped.
Una huesped es aquella persona que reside temporalmente en el territorio.
Aunque su estancia es de duración incierta o inferior al mes.
Tendrá una cuota de mantenimiento de 30€ al mes, con posibilidad de modificarla.
Una huesped puede convertirse en invitada si hace un pacto claro con la asamblea.
Para esto se cierra la estancia como huesped y se crea una invitada nueva.

### Atributos
* Heredados de ```Persona```

PersonaTrastero
-----------------

### Descripción
El modelo que recoge la información preciosa de la relación entre un ```Trastero``` y una ```Persona```.

### Atributos
* Trastero (One to One)
* Persona (One to One)
* fecha de entrada
* fecha de salida

HabitantePiso
-------------

### Descripción
El modelo que recoge la información preciosa de la relación entre un ```Piso``` y una ```Habitante```.

### Atributos
* Habitante (One to One)
* Piso (One to One)
* fecha de entrada
* fecha de salida
* total acumulado personal

Registro
--------

### Descripción
Clase genérica para los difertes registros contables:
* Métalico

* Banco

* Paypal

### Atributos
* nombre

* saldo

Métalico
--------

### Hereda de
```Registro```

### Descripción
El modelo que recoge la información del registro de movimientos en metálico.

### Atributos
* Heredados de ```Registro```

Banco
-----

### Hereda de
```Registro```

### Descripción
El modelo que recoge la información del registro de un banco.

### Atributos
* Heredados de ```Registro```

* número de cuenta

PayPal
------

### Hereda de 
```Registro```

### Descripción
El modelo que recoge la información del registro de una cuenta de PayPal.

### Atributos
* Heredados de ```Registro```

* nombre de usuario

Entrada
-------

### Descripción
Clase genérica para:

* Entrada piso

* Entrada alquiler

* Entrada manutención

* Entrada donativo

* Entrada evento

* Entrada trastero

* Entrada espacio proyecto productivo

* Entrada beneficio proyecto productivo

* Otra entrada

### Atributos
* fecha

* importe

* comentarios

* usuario gt economía que inserta los datos (Foreign Key)

* Registro (Foreign Key)

Entrada Mantenimiento
---------------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de mantenimiento.
Las aportadoras de una cuota de mantenimiento son cualquiera subclase de ```Persona```.

### Atributos
* Heredados de ```Entrada```

* Persona (Foreign Key)

Entrada Piso
------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de una cuota de piso.
Las aportadoras de una cuota piso son excluvisvamente habitantes.

### Atributos
* Heredados de ```Entrada```

* HabitantePiso (Foreign Key)

Entrada Alquiler
----------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de alquiler, ya sea de una habitación que de un piso.
Las aportadoras de una cuota de alquiler pueden ser invitadas o habitantes.

### Atributos
* Heredados de ```Entrada```

* Persona (Foreign Key)

Entrada Donativo
----------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada-donación.

### Atributos
* Heredados de ```Entrada```

* Persona (Foreign Key)

Entrada Beneficios Evento
-------------------------

### Hereda de 
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de beneficios de un evento.

### Atributos
* Heredados de ```Entrada```

Otra Entrada
------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de cualquier otro tipo de entrada no tipificada por el sistema.

### Atributos
* Heredados de ```Entrada```

Entrada Espacio Proyecto Productivo
-----------------------------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de alquiler de un espacio de un proyecto productivo.

### Atributos
* Heredados de ```Entrada```

* ProyectoProductivo (Foreign Key)

Entrada Beneficios Proyecto Productivo
--------------------------------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada del beneficio de un proyecto productivo.

### Atributos
* Heredados de ```Entrada```

* Proyecto Productivo (Foreign Key)

Entrada Alquiler Trastero
-------------------------

### Hereda de
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de alquiler de un trastero.

### Atributos
* Heredados de ```Entrada```

* HabitanteTrastero (Foreign Key)

Entrada Transferencia
---------------------

### Hereda de 
```Entrada```

### Descripción
El modelo que recoge la información de una entrada de tranferencia desde un banco a otro banco.

### Atributos
* Heredados de ```Entrada```

* Banco (Foreign Key)

Salida
------

### Descripción
El modelo que recoge la información de una salida.

### Atributos
* fecha

* importe

* comentarios

* tipo de salida

* usuario gt economía que inserta los datos (Foreign Key)

* Registro (Foreign Key)

Balance
-------

### Descripción
El modelo que recoge la información de un balance.

### Atributos
* nombre

* descripción

* fecha de inicio

* fecha de final

* Entrada (Many to Many)

* Salida (Many to Many)

* subtotal
