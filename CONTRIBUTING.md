# Contributing

## Git workflow

1. Fork this repo
2. Clone the fork in your local system
3. Add the main repo as a remote repository to be able to receive upstrem changes
```
git remote add main git@gitlab.com:calafou/eco-gnomy.git
```

## General recommendations:

* Work in branches, it is preferably
* Push the branch that you're working on to your fork.
* From the web interface create a Merge Request.
* Wait until its reviewed.
* Keep up to date your master:

```
git pull main master
```
