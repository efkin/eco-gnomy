"""ecognome URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    # Admin related URLs
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    # Frontend related URLs
    url(r'^foundation/', include('foundation.urls')),
    # Login related URLs
    url(r'^accounts/', include([
        url(r'^login/$', auth_views.login, name='login'),
        url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
        # Password related URLs
        url(r'^password/', include([
            url(r'^change/$', auth_views.password_change, name='password_change'),
            url(r'^change/done/$', auth_views.password_change_done, name='password_change_done'),
            ])),
        ])),
    # Druids app
    url(r'^', include('druids.urls', namespace='druids')),
]
