from django import forms
from druids.models import Habitante


class SelectHabitanteForm(forms.Form):
    """
    Just a single select field with all the habitants.
    """
    habitante = forms.ModelChoiceField(required=True, label="Escoge un habitante",
                                       queryset=Habitante.objects.all())

