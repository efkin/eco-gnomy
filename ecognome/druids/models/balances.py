from django.db import models
from .entradas import Entrada
from .salidas import Salida


class Balance(models.Model):
    """
    El modelo que recoge la información de un balance.
    """
    nombre = models.CharField(max_length=256)
    descripcion = models.CharField(max_length=512, verbose_name="Descripción")
    fecha_de_inicio = models.DateField()
    fecha_de_final = models.DateField()

    def _get_entradas(self):
        return Entrada.objects.filter(fecha__range=[self.fecha_de_inicio, self.fecha_de_final])

    def _get_salidas(self):
        return Salida.objects.filter(fecha__range=[self.fecha_de_inicio, self.fecha_de_final])

    def _get_subtotal(self):
        return  (sum([entrada.importe for entrada in self.entradas]) - sum([salida.importe for salida in self.salidas]))
        
    entradas = property(_get_entradas)
    salidas = property(_get_salidas)
    subtotal = property(_get_subtotal)

    def __str__(self):
        return ("%s: %s - %s" % (self.nombre, self.descripcion, str(self.subtotal)))

    
    
