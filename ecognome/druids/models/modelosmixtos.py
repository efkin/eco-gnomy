from django.db import models
from .espacios import Trastero, Piso
from .personas import Persona, Habitante

from datetime import datetime
from dateutil.relativedelta import relativedelta


class PersonaTrastero(models.Model):
    """
    El modelo que recoge la información preciosa de la
    relación entre un Trastero y una Persona.
    """
    trastero = models.ForeignKey(Trastero)
    persona = models.ForeignKey(Persona)
    fecha_entrada = models.DateField(verbose_name="Fecha de entrada")
    fecha_salida = models.DateField(blank=True, null=True, verbose_name="Fecha de salida")

    def _is_active(self):
        return not isinstance(self.fecha_salida, datetime)

    is_active = property(_is_active)
    
    def _cuotas_pagadas(self):
        return int(sum([entrada.importe for entrada in self.entradas_alquiler.all()]) / self.trastero.cuota_trastero)

    cuotas_pagadas = property(_cuotas_pagadas)

    def _deuda(self):
        had_paid = sum([entrada.importe for entrada in self.entradas_alquiler.all()])
        if self.is_active:
            relative_delta = relativedelta(datetime.now(), self.fecha_entrada)
            should_had_paid = (relative_delta.months + relative_delta.years * 12) * self.trastero.cuota_trastero
            return should_had_paid - had_paid

        else:
            relative_delta = relativedelta(self.fecha_salida, self.fecha_entrada)
            should_had_paid = (relative_delta.months + relative_delta.years * 12) * self.trastero.cuota_trastero
            return should_had_paid - had_paid

    deuda = property(_deuda)
    
    def __str__(self):
        return ("%s - %s" % (self.trastero.nombre, self.persona.nombre))


class HabitantePiso(models.Model):
    """
    El modelo que recoge la información preciosa de la
    relación entre un Piso y una Habitante.
    """
    habitante = models.ForeignKey(Habitante)
    piso = models.ForeignKey(Piso)
    fecha_entrada = models.DateField(verbose_name="Fecha de entrada")
    fecha_salida = models.DateField(blank=True, null=True, verbose_name="Fecha de salida")

    entrada_pagada = models.BooleanField(default=False, verbose_name="Cuota de entrada?")
    onduline_pagado = models.BooleanField(default=False, verbose_name="Cuota de onduline?")

    def _is_active(self):
        return not isinstance(self.fecha_salida, datetime)

    is_active = property(_is_active)
    
    def _cuotas_pagadas(self):
        return int(sum([entrada.importe for entrada in self.entradas_piso.all()]) / int(self.piso.cuota_piso.importe))

    cuotas_pagadas = property(_cuotas_pagadas)

    def _deuda(self):
        had_paid = sum([entrada.importe for entrada in self.entradas_piso.all()])
        if self.is_active:
            relative_delta = relativedelta(datetime.now(), self.fecha_entrada)
            should_had_paid =  (relative_delta.months + relative_delta.years * 12) * self.piso.cuota_piso.importe
            return should_had_paid - had_paid

        else:
            relative_delta = relativedelta(self.fecha_salida, self.fecha_entrada)
            should_had_paid = (relative_delta.months + relative_delta.years * 12) * self.piso.cuota_piso.importe
            return should_had_paid - had_paid

    deuda = property(_deuda)

    def _porcentaje_pagado(self):
        return int(sum([entrada.importe for entrada in self.entradas_piso.all()]) * 100 / self.piso.valor_total)

    porcentaje_pagado = property(_porcentaje_pagado)
    
    def __str__(self):
        return ("%s - %s" % (self.piso.nombre, self.habitante.nombre))
