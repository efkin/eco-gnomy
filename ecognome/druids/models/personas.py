from django.db import models
from .cuotas import CuotaMantenimiento
from datetime import datetime
from dateutil.relativedelta import relativedelta


class Persona(models.Model):
    """
    Clase genérica para habitante, invitado y huesped.
    """
    nombre = models.CharField(max_length=256)
    cuota_mantenimiento = models.ForeignKey(CuotaMantenimiento, related_name='+')
    fecha_de_entrada = models.DateField()
    fecha_de_salida = models.DateField(blank=True, null=True)

    def _is_active(self):
        return not isinstance(self.fecha_de_salida, datetime)

    is_active = property(_is_active)

    def _cuotas_mantenimiento_pagadas(self):
        return int(sum([entrada.importe for entrada in self.entradas_mantenimiento.all()]) / int(self.cuota_mantenimiento.importe))

    cuotas_mantenimiento_pagadas = property(_cuotas_mantenimiento_pagadas)
    
    def _deuda_mantenimiento(self):
        had_paid = sum([entrada.importe for entrada in self.entradas_mantenimiento.all()])
        if self.is_active:
            relative_delta = relativedelta(datetime.now(), self.fecha_de_entrada)
            should_had_paid = (relative_delta.months + relative_delta.years * 12) * self.cuota_mantenimiento.importe
            return should_had_paid - had_paid

        else:
            relative_delta = relativedelta(self.fecha_de_salida, self.fecha_de_entrada)
            should_had_paid =  (relative_delta.months + relative_delta.years * 12) * self.cuota_mantenimiento.importe
            return should_had_paid - had_paid

    deuda_mantenimiento = property(_deuda_mantenimiento)

    def _tiene_pacto_trastero(self):
        return self.personatrastero_set.count() != 0

    tiene_pacto_trastero = property(_tiene_pacto_trastero)

    def _estado_pactos_trastero(self):
        estado_pactos = []
        for pacto in self.personatrastero_set.all():
            estado_pactos += [
                {
                    'nombre': pacto.trastero.nombre,
                    'usando': pacto.is_active,
                    'fecha_entrada': pacto.fecha_entrada,
                    'fecha_salida': pacto.fecha_salida,
                    'cuotas_pagadas': pacto.cuotas_pagadas,
                    'deuda': pacto.deuda,
                }
            ]
        return estado_pactos

    estado_pactos_trastero = property(_estado_pactos_trastero)

        
    def __str__(self):
        return self.nombre


class Habitante(Persona):
    """
    El modelo que recoge la información sobre una habitante.
    Una habitante es aquella persona que reside
    permanentemente en el territorio.
    Ya sea en viviendas, casa roja o el camping.
    Tendrá una cuota de mantenimiento de 10€ al mes,
    con posibilidad de modificarla.
    """
    def _tiene_pacto_de_piso(self):
        return self.habitantepiso_set.count() != 0

    tiene_pacto_de_piso = property(_tiene_pacto_de_piso)

    def _estado_pactos_de_piso(self):
        estado_pactos = []
        for pacto in self.habitantepiso_set.all():
            estado_pactos += [
                {
                    'nombre': pacto.piso.nombre,
                    'viviendo': pacto.is_active,
                    'fecha_entrada': pacto.fecha_entrada,
                    'fecha_salida': pacto.fecha_salida,
                    'cuotas_pagadas': pacto.cuotas_pagadas,
                    'deuda': pacto.deuda,
                    'porcentaje_pagado': pacto.porcentaje_pagado,
                }
            ]
        return estado_pactos

    estado_pactos_de_piso = property(_estado_pactos_de_piso)


class Invitada(Persona):
    """
    El modelo que recoge la información sobre una invitada.
    Una invitada es aquella persona que reside temporalmente
    en el territorio.
    Aunque su estancia es mínimo de un mes.
    Tendrá una cuota de mantenimiento de 15€ al mes,
    con posiblidad de modificarla.
    Una invitada puede convertirse en habitante.
    Para esto se cierra la estancia como invitada
    y se crea una habitante nueva.
    """
    pass


class Huesped(Persona):
    """
    El modelo que recoge la información sobre una huesped.
    Una huesped es aquella persona que reside temporalmente
    en el territorio.
    Aunque su estancia es de duración incierta o inferior al mes.
    Tendrá una cuota de mantenimiento de 30€ al mes,
    con posibilidad de modificarla.
    Una huesped puede convertirse en invitada si hace un pacto
    claro con la asamblea.
    Para esto se cierra la estancia como huesped y se crea una
    invitada nueva.
    """
    class Meta:
        verbose_name_plural = "Huespedes"

