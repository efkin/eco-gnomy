from django.db import models
from django.contrib.auth.models import User
from .registros import Registro, Banco
from .personas import Persona
from .modelosmixtos import HabitantePiso, PersonaTrastero
from .proyectos import ProyectoProductivo


class Entrada(models.Model):
    """
    Clase genérica para una entrada en un Registro
    """
    MANTENIMIENTO = "mantenimiento"
    PISO = "piso"
    ALQUILER = "alquiler"
    DONATIVO = "donativo"
    EVENTO = "evento"
    ESPACIO_PP = "espacio_pp"
    BENEFICIO_PP = "beneficio_pp"
    TRASTERO = "trastero"
    OTRA = "otra"
    TRANSFERENCIA = "transferencia"
    
    TIPOS_ENTRADA = (
        (MANTENIMIENTO, "Mantenimiento"),
        (PISO, "Cuota de Piso"),
        (ALQUILER, "Alquiler"),
        (DONATIVO, "Donativo"),
        (EVENTO, "Beneficios Evento"),
        (ESPACIO_PP, "Alquiler Espacio Proy. Prod."),
        (TRASTERO, "Alquiler de Trastero"),
        (OTRA, "Otra Entrada"),
        (TRANSFERENCIA, "Transferencia"),
    )
    
    fecha = models.DateField()
    importe = models.DecimalField(max_digits=7, decimal_places=2)
    comentarios = models.CharField(max_length=512, blank=True, null=True)
    usuaria_economia = models.ForeignKey(User, verbose_name="Usuaria de economía")
    registro = models.ForeignKey(Registro)
    tipo = models.CharField(max_length=64, choices=TIPOS_ENTRADA)


class EntradaMantenimiento(Entrada):
    """
    El modelo que recoge la información de una entrada
    de mantenimiento.
    Las aportadoras de una cuota de mantenimiento son
    cualquiera subclase de Persona.
    """
    persona = models.ForeignKey(Persona, related_name="entradas_mantenimiento")

    def __str__(self):
        return "Mantenimiento"
    
    class Meta:
        verbose_name = "Entrada de Mantenimiento"
        verbose_name_plural = "Entradas de Mantenimiento"

    
class EntradaPiso(Entrada):
    """
    El modelo que recoge la información de una entrada
    de una cuota de piso.
    Las aportadoras de una cuota piso son excluvisvamente
    habitantes.
    """
    habitante_piso = models.ForeignKey(HabitantePiso, related_name="entradas_piso")

    def __str__(self):
        return "Piso"
    
    class Meta:
        verbose_name = "Entrada de Piso"
        verbose_name_plural = "Entradas de Piso"


class EntradaAlquiler(Entrada):
    """
    El modelo que recoge la información de una entrada
    de alquiler, ya sea de una habitación que de un piso.
    Las aportadoras de una cuota de alquiler pueden ser
    invitadas o habitantes.
    """
    persona = models.ForeignKey(Persona, related_name="entradas_alquiler")

    def __str__(self):
        return "Alquiler"

    
    
    class Meta:
        verbose_name = "Entrada de Alquiler"
        verbose_name_plural = "Entradas de Alquiler"


class EntradaDonativo(Entrada):
    """
    El modelo que recoge la información de una entrada-donación.
    """
    persona = models.ForeignKey(Persona, related_name="entradas_donativos")

    def __str__(self):
        return "Donativo"
    
    class Meta:
        verbose_name = "Entrada de Donativo"
        verbose_name_plural = "Entradas de Donativo"


class EntradaBeneficiosEvento(Entrada):
    """
    El modelo que recoge la información de una entrada de
    beneficios de un evento.
    """

    def __str__(self):
        return "Beneficios Evento"

    
    
    class Meta:
        verbose_name = "Entrada de Beneficios de un Evento"
        verbose_name_plural = "Entradas de Beneficios de un Evento"


class OtraEntrada(Entrada):
    """
    El modelo que recoge la información de cualquier otro
    tipo de entrada no tipificada por el sistema.
    """

    def __str__(self):
        return "Otras entradas"
    
    class Meta:
        verbose_name = "Otra Entrada"
        verbose_name_plural = "Otras Entradas"


class EntradaEspacioProyectoProductivo(Entrada):
    """
    El modelo que recoge la información de una entrada de
    alquiler de un espacio de un proyecto productivo.
    """
    proyecto_productivo = models.ForeignKey(ProyectoProductivo, related_name="entradas_espacio")

    def __str__(self):
        return "Alquiler Espacio Proyecto Productivo"
    
    class Meta:
        verbose_name = "Entrada por el Espacio de un Proyecto Productivo"
        verbose_name_plural = "Entradas por el Espacio de un Proyecto Productivo"


class EntradaBeneficioProyectoProductivo(Entrada):
    """
    El modelo que recoge la información de una entrada del
    beneficio de un proyecto productivo.
    """
    proyecto_productivo = models.ForeignKey(ProyectoProductivo, related_name="entradas_beneficio")

    def __str__(self):
        return "Beneficio Proyecto Productivo"
    
    class Meta:
        verbose_name = "Entrada de Beneficio de un Proyecto Productivo"
        verbose_name_plural = "Entradas de Beneficios de un Proyecto Productivo"


class EntradaAlquilerTrastero(Entrada):
    """
    El modelo que recoge la información de una entrada de
    alquiler de un trastero.
    """
    persona_trastero = models.ForeignKey(PersonaTrastero, related_name="entradas_alquiler")

    def __str__(self):
        return "Alquiler Trastero"
    
    class Meta:
        verbose_name = "Entrada de Alquiler de un Trastero"
        verbose_name_plural = "Entradas de Alquiler de un Trastero"


class EntradaTransferencia(Entrada):
    """
    El modelo que recoge la información de una entrada de
    tranferencia desde un banco a otro banco.
    """

    def __str__(self):
        return "Transferencia entre Cuentas"
    
    class Meta:
        verbose_name = "Entrada de Transferencia"
        verbose_name_plural = "Entradas de Transferencia"


