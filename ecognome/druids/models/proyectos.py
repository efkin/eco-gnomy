from django.db import models


class ProyectoProductivo(models.Model):
    """
    El modelo que recoge la información sobre un proyecto productivo.
    """
    nombre = models.CharField(max_length=256)
    cuota_espacio = models.DecimalField(max_digits=5, decimal_places=2)
    fecha_creacion = models.DateField(verbose_name="Fecha de creación del proyecto")
    fecha_disolucion = models.DateField(verbose_name="Fecha de disolución del proyecto",
                                        null=True,
                                        blank=True)
    
    def __str__(self):
        return self.nombre

