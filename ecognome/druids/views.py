from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory, modelformset_factory
from django.forms.widgets import Select
from django.http import JsonResponse

from .forms import SelectHabitanteForm
from .models import CuotaMantenimiento, CuotaPiso, \
    Piso, Trastero, \
    Metalico, Banco, PayPal, Registro, \
    ProyectoProductivo, \
    Habitante, Invitada, Huesped, \
    HabitantePiso, PersonaTrastero, \
    Salida, \
    EntradaMantenimiento, EntradaPiso, EntradaAlquiler, \
    EntradaDonativo, EntradaBeneficiosEvento, OtraEntrada, \
    EntradaEspacioProyectoProductivo, EntradaBeneficioProyectoProductivo, \
    EntradaAlquilerTrastero, EntradaTransferencia, Entrada, \
    Balance

from datetime import datetime, timedelta


def index(request):
    """
    Index dahsboard for druids app.
    """
    return render(
        request,
        'druids/index.html'
        )


def situaciones_personales(request, habitante_id=None):
    """
    This view renders a form that is used to update the content
    of the view. If `habitante_id` is None only the form will be rendered.
    Else, all the relevant information about specific `Habitante` will be rendered.
    """

    title = "Situaciones Personales"

    habitante = None
    estado_pactos_de_piso = None
    estado_pactos_trastero = None
    if request.method == 'POST':
        form = SelectHabitanteForm(request.POST)
        if form.is_valid():
            selected = form.cleaned_data['habitante']
            habitante_id = selected.id
            return redirect('druids:situaciones-personales', habitante_id=habitante_id)

    else:
         form = SelectHabitanteForm()
         if habitante_id is not None:
             habitante = get_object_or_404(Habitante, id=habitante_id)
             if habitante.tiene_pacto_trastero:
                 estado_pactos_trastero = habitante.estado_pactos_trastero
             if habitante.tiene_pacto_de_piso:
                 estado_pactos_de_piso = habitante.estado_pactos_de_piso

    return render(request,
                  'druids/situaciones_personales.html',
                  {
                      'title': title,
                      'habitante': habitante,
                      'form': form,
                      'estado_pactos_trastero': estado_pactos_trastero,
                      'estado_pactos_de_piso': estado_pactos_de_piso,
                  }
              )


def balance_general(request):
    """
    Main balance in accounting view.
    """
    title = "Balance general"

    total = str(sum([entrada.importe for entrada in Entrada.objects.all()]) - sum([salida.importe for salida in Salida.objects.all()]))

    return render(request,
                  'druids/balance_general.html',
                  {
                      'title': title,
                      'total': total,
                  }
              )

             
@login_required
def gt_dashboard(request):
    """
    Dashboard for GT Economía
    """
    return render(
        request,
        'druids/gt-dashboard.html'
        )


class CuotaMantenimientoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for CuotaMantenimiento
    model.
    """
    model = CuotaMantenimiento
    template_name = 'druids/cuotas/crear_mantenimiento.html'
    form_class = modelform_factory(CuotaMantenimiento,
                                   fields=['descripcion', 'tipo', 'importe']
                               )
    success_url = '/gt-economia/'
    success_message = 'La cuota de mantenimiento se ha creado correctamente.'

    title = 'Crear cuota de mantenimiento'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaMantenimientoCreateView, self).dispatch(*args, **kwargs)


class CuotaMantenimientoListView(ListView):
    """
    List View for CuotaMantenimiento model.
    """
    context_object_name = 'cuotas'
    queryset = CuotaMantenimiento.objects.all().order_by('tipo')
    template_name = "druids/cuotas/lista_mantenimiento.html"
    title = 'Lista de cuotas de mantenimiento'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaMantenimientoListView, self).dispatch(*args, **kwargs)


class CuotaPisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for CuotaPiso
    model.
    """
    model = CuotaPiso
    template_name = 'druids/cuotas/crear_mantenimiento.html'
    form_class = modelform_factory(CuotaPiso,
                                   fields=['importe', 'descripcion']
                               )
    success_url = '/gt-economia/'
    success_message = 'La cuota de piso se ha creado correctamente.'

    title = 'Crear cuota de piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaPisoCreateView, self).dispatch(*args, **kwargs)


class CuotaPisoListView(ListView):
    """
    List View for CuotaMantenimiento model.
    """
    context_object_name = 'cuotas'
    queryset = CuotaPiso.objects.all().order_by('importe')
    template_name = "druids/cuotas/lista_cuotas_piso.html"
    title = 'Lista de cuotas de piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CuotaPisoListView, self).dispatch(*args, **kwargs)


class PisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Piso model.
    """
    model = Piso
    template_name = "druids/espacios/crear-piso.html"
    form_class = modelform_factory(Piso,
                                   fields=['nombre', 'valor_total', 'cuota_piso']
                               )
    success_url = '/gt-economia/'
    success_message = 'El piso se ha creado correctamente.'

    title = 'Crear piso'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoCreateView, self).dispatch(*args, **kwargs)


class PisoListView(ListView):
    """
    List View for Piso model.
    """
    context_object_name = 'pisos'
    queryset = Piso.objects.all().order_by('nombre')
    template_name = "druids/espacios/lista-pisos.html"
    title = "Lista de pisos"

    edit_url = "druids:editar-piso"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoListView, self).dispatch(*args, **kwargs)


class PisoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Piso model.
    """
    model = Piso
    template_name = "druids/espacios/crear-piso.html"
    fields = ['nombre', 'valor_total', 'cuota_piso']
    
    title = "Modificar piso"

    success_url = "/gt-economia/"
    success_message = "El piso se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PisoUpdateView, self).dispatch(*args, **kwargs)


class TrasteroCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Trastero model.
    """
    model = Trastero
    template_name = "druids/espacios/crear-trastero.html"
    form_class = modelform_factory(Trastero,
                                   fields=['nombre', 'cuota_trastero']
                               )
    success_url = '/gt-economia/'
    success_message = 'El trastero se ha creado correctamente.'

    title = 'Crear trastero'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroCreateView, self).dispatch(*args, **kwargs)


class TrasteroListView(ListView):
    """
    List View for Trastero model.
    """
    context_object_name = 'trasteros'
    queryset = Trastero.objects.all().order_by('nombre')
    template_name = "druids/espacios/lista-trasteros.html"
    title = "Lista de trasteros"

    edit_url = "druids:editar-trastero"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroListView, self).dispatch(*args, **kwargs)


class TrasteroUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Trastero model.
    """
    model = Trastero
    template_name = "druids/espacios/crear-trastero.html"
    fields = ['nombre', 'cuota_trastero']
    
    title = "Modificar trastero"

    success_url = "/gt-economia/"
    success_message = "El trastero se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TrasteroUpdateView, self).dispatch(*args, **kwargs)


class MetalicoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Metalico model.
    """
    model = Metalico
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(Metalico,
                                   fields=["nombre"]
                               )

    title = "Crear registro de metálico"
    
    success_url = "/gt-economia/"
    success_message = "El registro de metálico se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MetalicoCreateView, self).dispatch(*args, **kwargs)
    

class MetalicoListView(ListView):
    """
    List View for Metalico model.
    """
    context_object_name = "registros"
    queryset = Metalico.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros de metálico"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MetalicoListView, self).dispatch(*args, **kwargs)


class BancoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Banco model.
    """
    model = Banco
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(Banco,
                                   fields=["nombre"]
                               )

    title = "Crear registro bancario"
    
    success_url = "/gt-economia/"
    success_message = "El registro bancario se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BancoCreateView, self).dispatch(*args, **kwargs)
    

class BancoListView(ListView):
    """
    List View for Banco model.
    """
    context_object_name = "registros"
    queryset = Banco.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros bancarios"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BancoListView, self).dispatch(*args, **kwargs)


class PaypalCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for PayPal model.
    """
    model = PayPal
    template_name = "druids/registros/crear_registro.html"
    form_class = modelform_factory(PayPal,
                                   fields=["nombre"]
                               )

    title = "Crear registro electrónico"
    
    success_url = "/gt-economia/"
    success_message = "El registro electrónico se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PaypalCreateView, self).dispatch(*args, **kwargs)
    

class PaypalListView(ListView):
    """
    List View for PayPal model.
    """
    context_object_name = "registros"
    queryset = PayPal.objects.all().order_by('nombre')
    template_name = "druids/registros/lista-registros.html"
    title = "Lista de registros electrónicos"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PaypalListView, self).dispatch(*args, **kwargs)


class ProyectoProductivoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for ProyectoProductivo model.
    """
    model = ProyectoProductivo
    template_name = "druids/proyectos/crear-proyecto.html"
    form_class = modelform_factory(ProyectoProductivo,
                                   fields=["nombre", "cuota_espacio", "fecha_creacion"]
                               )

    title = "Crear proyecto productivo"

    success_url = "/gt-economia/"
    success_message = "El proyecto productivo se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoCreateView, self).dispatch(*args, **kwargs)


class ProyectoProductivoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for ProyectoProductivo model.
    """
    model = ProyectoProductivo
    template_name = "druids/proyectos/editar-proyecto.html"
    fields = ['cuota_espacio', 'fecha_disolucion']
    
    title = "Modificar proyecto"

    success_url = "/gt-economia/"
    success_message = "El proyecto se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoUpdateView, self).dispatch(*args, **kwargs)


class ProyectoProductivoListView(ListView):
    """
    List View for ProyectoProductivo model.
    """
    context_object_name = "proyectos"
    queryset = ProyectoProductivo.objects.all().order_by('nombre')
    template_name = "druids/proyectos/lista-proyectos.html"
    title = "Lista de proyectos productivos"

    edit_url = "druids:editar-proyecto"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProyectoProductivoListView, self).dispatch(*args, **kwargs)
    

class HabitanteCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Habitante model.
    """
    model = Habitante
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(Habitante,
                                   fields=["nombre", "cuota_mantenimiento",
                                           "fecha_de_entrada"]
                               )

    title = "Crear habitante"

    success_url = "/gt-economia/"
    success_message = "La habitante se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteCreateView, self).dispatch(*args, **kwargs)


class HabitanteUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Habitante model.
    """
    model = Habitante
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuota_mantenimiento', 'fecha_de_salida']
    
    title = "Modificar habitante"

    success_url = "/gt-economia/"
    success_message = "La habitante se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteUpdateView, self).dispatch(*args, **kwargs)


class HabitanteListView(ListView):
    """
    List View for Habitante model.
    """
    context_object_name = "personas"
    queryset = Habitante.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de habitantes"

    edit_url = "druids:editar-habitante"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitanteListView, self).dispatch(*args, **kwargs)
    

class InvitadaCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Invitada model.
    """
    model = Invitada
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(Invitada,
                                   fields=["nombre", "cuota_mantenimiento",
                                           "fecha_de_entrada"]
                               )

    title = "Crear invitada"

    success_url = "/gt-economia/"
    success_message = "La invitada se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaCreateView, self).dispatch(*args, **kwargs)


class InvitadaUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Invitada model.
    """
    model = Invitada
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuota_mantenimiento', 'fecha_de_salida']
    
    title = "Modificar invitada"

    success_url = "/gt-economia/"
    success_message = "La invitada se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaUpdateView, self).dispatch(*args, **kwargs)


class InvitadaListView(ListView):
    """
    List View for Invitada model.
    """
    context_object_name = "personas"
    queryset = Invitada.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de invitadas"

    edit_url = "druids:editar-invitada"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(InvitadaListView, self).dispatch(*args, **kwargs)


class HuespedCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Huesped model.
    """
    model = Huesped
    template_name = "druids/personas/crear-persona.html"
    form_class = modelform_factory(Huesped,
                                   fields=["nombre", "cuota_mantenimiento",
                                           "fecha_de_entrada"]
                               )

    title = "Crear huesped"

    success_url = "/gt-economia/"
    success_message = "La huesped se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedCreateView, self).dispatch(*args, **kwargs)


class HuespedUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for Huesped model.
    """
    model = Huesped
    template_name = "druids/personas/editar-persona.html"
    fields = ['cuota_mantenimiento', 'fecha_de_salida']
    
    title = "Modificar huesped"

    success_url = "/gt-economia/"
    success_message = "La huesped se ha modificado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedUpdateView, self).dispatch(*args, **kwargs)


class HuespedListView(ListView):
    """
    List View for Huesped model.
    """
    context_object_name = "personas"
    queryset = Huesped.objects.all().order_by('nombre')
    template_name = "druids/personas/lista-personas.html"
    title = "Lista de huéspedes"

    edit_url = "druids:editar-huesped"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HuespedListView, self).dispatch(*args, **kwargs)


class HabitantePisoCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for HabitantePiso model.
    """
    model = HabitantePiso
    template_name = "druids/pactos/crear-habitantepiso.html"
    form_class = modelform_factory(HabitantePiso,
                                   fields=["habitante", "piso",
                                           "fecha_entrada", "entrada_pagada",
                                           "onduline_pagado"]
                               )

    title = "Crear pacto de reciprocidad"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoCreateView, self).dispatch(*args, **kwargs)


class HabitantePisoUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for HabitantePiso model.
    """
    model = HabitantePiso
    template_name = "druids/pactos/editar-pacto.html"
    fields = ['fecha_salida', "entrada_pagada", "onduline_pagado"]

    kind_of = "piso"
    
    title = "Concluir pacto"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha cerrado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoUpdateView, self).dispatch(*args, **kwargs)


class HabitantePisoListView(ListView):
    """
    List View for HabitantePiso model.
    """
    context_object_name = "pactos"
    queryset = HabitantePiso.objects.all().order_by('fecha_entrada')
    template_name = "druids/pactos/lista-habitantepiso.html"
    title = "Lista de pactos"

    edit_url = "druids:editar-habitantepiso"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HabitantePisoListView, self).dispatch(*args, **kwargs)


class PersonaTrasteroCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for PersonaTrastero model.
    """
    model = PersonaTrastero
    template_name = "druids/pactos/crear-personatrastero.html"
    form_class = modelform_factory(PersonaTrastero,
                                   fields=["persona", "trastero",
                                           "fecha_entrada"]
                               )

    title = "Crear pacto de reciprocidad"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha creado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroCreateView, self).dispatch(*args, **kwargs)


class PersonaTrasteroUpdateView(SuccessMessageMixin, UpdateView):
    """
    Update View for PersonaTrastero model.
    """
    model = PersonaTrastero
    template_name = "druids/pactos/editar-pacto.html"
    fields = ['fecha_salida']

    kind_of = "trastero"
    
    title = "Concluir pacto"

    success_url = "/gt-economia/"
    success_message = "El pacto se ha cerrado correctamente."

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroUpdateView, self).dispatch(*args, **kwargs)


class PersonaTrasteroListView(ListView):
    """
    List View for PersonaTrastero model.
    """
    context_object_name = "pactos"
    queryset = PersonaTrastero.objects.all().order_by('fecha_entrada')
    template_name = "druids/pactos/lista-personatrastero.html"
    title = "Lista de pactos"

    edit_url = "druids:editar-personatrastero"
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonaTrasteroListView, self).dispatch(*args, **kwargs)


@login_required
def insertar_salidas(request):
    """
    Custom view to insert 'Salidas'
    """

    title = "Insertar salidas"
    
    SalidasModelFormSet = modelformset_factory(Salida, extra=9, min_num=1, validate_min=True,
                                               fields=["fecha", "tipo_salida",
                                           "importe", "comentarios",
                                           "registro", "usuaria_economia"],
                                           )
    if request.method == 'POST':
        formset = SalidasModelFormSet(data=request.POST)
        if formset.is_valid():
            formset.save()
            messages.success(request, "Las salidas se han insertado correctamente.")
            return redirect("druids:gt-dashboard")

    else:
        formset = SalidasModelFormSet(queryset=Salida.objects.none())

    return render(
        request,
        "druids/movimientos/crear-salidas.html",
        {'formset': formset,
         'title': title}
        )


@login_required
def insertar_entradas_otras(request):
    """
    Custom view to insert:
    * EntradaMantenimiento
    * EntradaDonativo
    * OtraEntrada
    * Transferencia
    """

    title = "Insertar otras entradas"

    MantenimientoModelFormSet = modelformset_factory(EntradaMantenimiento,
                                                     exclude=['tipo'])

    DonativoModelFormSet = modelformset_factory(EntradaDonativo,
                                                exclude=['tipo'])

    OtrasModelFormSet = modelformset_factory(OtraEntrada,
                                             exclude=['tipo'])

    TransferenciaModelFormSet = modelformset_factory(EntradaTransferencia,
                                                     exclude=['tipo'])

    if request.method == 'POST':
        mantenimiento_formset = MantenimientoModelFormSet(request.POST, prefix='mantenimiento')
        donativos_formset = DonativoModelFormSet(request.POST, prefix='donativos')
        otras_formset = OtrasModelFormSet(request.POST, prefix='otras')
        transferencias_formset = TransferenciaModelFormSet(request.POST, prefix='transferencias')

        if mantenimiento_formset.is_valid() and donativos_formset.is_valid() and \
           otras_formset.is_valid() and transferencias_formset.is_valid() :
            cuotas_m = mantenimiento_formset.save(commit=False)
            for cuota in cuotas_m:
                cuota.tipo = 'mantenimiento'
                cuota.save()
            donativos = donativos_formset.save(commit=False)
            for donativo in donativos:
                donativo.tipo = 'donativo'
                donativo.save()
            otras = otras_formset.save(commit=False)
            for otra in otras:
                otra.tipo = 'otra'
                otra.save()
            transferencias = transferencias_formset.save(commit=False)
            for transferencia in transferencias:
                transferencia.tipo = 'transferencia'
                transferencia.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        mantenimiento_formset = MantenimientoModelFormSet(queryset=EntradaMantenimiento.objects.none(),
                                                          prefix='mantenimiento')
        donativos_formset = DonativoModelFormSet(queryset=EntradaDonativo.objects.none(),
                                                 prefix='donativos')
        otras_formset = OtrasModelFormSet(queryset=OtraEntrada.objects.none(),
                                          prefix='otras')
        transferencias_formset = TransferenciaModelFormSet(queryset=EntradaTransferencia.objects.none(),
                                                           prefix='transferencias')

    return render(request, "druids/movimientos/crear-otras-entradas.html", {
        'title': title,
        'mantenimiento_formset': mantenimiento_formset,
        'donativos_formset': donativos_formset,
        'otras_formset': otras_formset,
        'transferencias_formset': transferencias_formset
        })


@login_required
def insertar_entradas_alquileres(request):
    """
    Custom view to insert:
    * EntradaPiso
    * EntradaAlquiler
    * EntradaAlquilerTrastero
    """

    title = "Insertar entradas de alquiler"

    PisoModelFormSet = modelformset_factory(EntradaPiso,
                                            exclude=['tipo'])

    AlquilerModelFormSet = modelformset_factory(EntradaAlquiler,
                                                exclude=['tipo'])

    AlquilerTrasteroModelFormSet = modelformset_factory(EntradaAlquilerTrastero,
                                                        exclude=['tipo'])

    if request.method == 'POST':
        pisos_formset = PisoModelFormSet(request.POST, prefix='pisos')
        alquileres_formset = AlquilerModelFormSet(request.POST, prefix='alquileres')
        trasteros_formset = AlquilerTrasteroModelFormSet(request.POST, prefix='trasteros')

        if pisos_formset.is_valid() and alquileres_formset.is_valid() and \
           trasteros_formset.is_valid() :
            pisos = pisos_formset.save(commit=False)
            for piso in pisos:
                piso.tipo = 'piso'
                piso.save()
            alquileres = alquileres_formset.save(commit=False)
            for alquiler in alquileres:
                alquiler.tipo = 'alquiler'
                alquiler.save()
            trasteros = trasteros_formset.save(commit=False)
            for trastero in trasteros:
                trastero.tipo = 'trastero'
                trastero.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        pisos_formset = PisoModelFormSet(queryset=EntradaPiso.objects.none(),
                                         prefix='pisos')
        alquileres_formset = AlquilerModelFormSet(queryset=EntradaAlquiler.objects.none(),
                                                  prefix='alquileres')
        trasteros_formset = AlquilerTrasteroModelFormSet(queryset=EntradaAlquilerTrastero.objects.none(),
                                                         prefix='trasteros')
        
    return render(request, "druids/movimientos/crear-alquileres-entradas.html", {
        'title': title,
        'pisos_formset': pisos_formset,
        'alquileres_formset': alquileres_formset,
        'trasteros_formset': trasteros_formset,
        })


@login_required
def insertar_entradas_proyectos(request):
    """
    Custom view to insert:
    * EntradaEspacioProyectoProductivo
    * EntradaBeneficioProyectoProductivo
    * EntradaBeneficiosEvento
    """

    title = "Insertar entradas de proyectos productivos"

    EspacioProyectoProductivoModelFormSet = modelformset_factory(EntradaEspacioProyectoProductivo,
                                                                 exclude=['tipo'])
                                                                 

    BeneficioProyectoProductivoModelFormSet = modelformset_factory(EntradaBeneficioProyectoProductivo,
                                                                   exclude=['tipo'])

    BeneficiosEventoModelFormSet = modelformset_factory(EntradaBeneficiosEvento,
                                                        exclude=['tipo'])

    if request.method == 'POST':
        espacios_formset = EspacioProyectoProductivoModelFormSet(request.POST, prefix='espacios')
        beneficios_formset = BeneficioProyectoProductivoModelFormSet(request.POST, prefix='beneficios')
        eventos_formset = BeneficiosEventoModelFormSet(request.POST, prefix='eventos')

        if espacios_formset.is_valid() and beneficios_formset.is_valid() and \
           eventos_formset.is_valid() :
            espacios = espacios_formset.save(commit=False)
            for espacio in espacios:
                espacio.tipo = 'espacio_pp'
                espacio.save()
            beneficios = beneficios_formset.save(commit=False)
            for beneficio in beneficios:
                beneficio.tipo = 'beneficio_pp'
                beneficio.save()
            eventos = eventos_formset.save(commit=False)
            for evento in eventos:
                evento.tipo = 'evento'
                evento.save()
            messages.success(request, "Las entradas han sido insertadas correctamente")
            return redirect("druids:gt-dashboard")

    else:
        espacios_formset = EspacioProyectoProductivoModelFormSet(queryset=EntradaEspacioProyectoProductivo.objects.none(),
                                                                 prefix='espacios')
        beneficios_formset = BeneficioProyectoProductivoModelFormSet(queryset=EntradaBeneficioProyectoProductivo.objects.none(),
                                                                     prefix='beneficios')
        eventos_formset = BeneficiosEventoModelFormSet(queryset=EntradaBeneficiosEvento.objects.none(),
                                                       prefix='eventos')
        
    return render(request, "druids/movimientos/crear-proyectos-entradas.html", {
        'title': title,
        'espacios_formset': espacios_formset,
        'beneficios_formset': beneficios_formset,
        'eventos_formset': eventos_formset,
        })


class BalanceCreateView(SuccessMessageMixin, CreateView):
    """
    Create View for Balance model.
    """
    model = Balance
    template_name = 'druids/balances/crear-balance.html'
    form_class = modelform_factory(Balance,
                                   fields=['nombre', 'descripcion',
                                           'fecha_de_inicio', 'fecha_de_final']
                               )
    success_url = '/gt-economia/'
    success_message = 'El balance se ha creado correctamente.'

    title = 'Crear balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceCreateView, self).dispatch(*args, **kwargs)


class BalanceDetailView(DetailView):
    """
    Detail View for Balance model.
    """
    model = Balance
    template_name = 'druids/balances/ver-balance.html'
    context_object_name = 'balance'
    
    title = 'Ver balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceDetailView, self).dispatch(*args, **kwargs)


class BalanceListView(ListView):
    """
    List View for Balance model.
    """
    context_object_name = 'balances'
    queryset = Balance.objects.all().order_by('nombre')
    template_name = "druids/balances/lista-balances.html"
    title = 'Lista de balances'

    edit_url = 'druids:ver-balance'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BalanceListView, self).dispatch(*args, **kwargs)


### API's views ###

def get_dashboard_data(request):
    """
    It returns a json containing the graph representation
    of the database. It will be used to render
    a circle packing graph.
    """
    data = {
        "name": "Calafou",
        "children": [
            {"name": "Habitantes",
             "children": [{"name": habitante.nombre,
                           "size": 1} for habitante in Habitante.objects.all()]},
            {"name": "Invitadas",
             "children": [{"name": invitada.nombre,
                           "size": 1} for invitada in Invitada.objects.all()]},
            {"name": "Huespedes",
             "children": [{"name": huesped.nombre,
                           "size": 1} for huesped in Huesped.objects.all()]},
            {"name": "Pisos",
             "children": [{"name": piso.nombre,
                           "size": 1} for piso in Piso.objects.all()]},
            {"name": "Trasteros",
             "children": [{"name": trastero.nombre,
                           "size": 1} for trastero in Trastero.objects.all()]},
            {"name": "Proyectos",
             "children": [{"name": proyecto.nombre,
                           "size": 1} for proyecto in ProyectoProductivo.objects.all()]},
            {"name": "Habitantes en Piso",
             "children": [{"name": str(habitantepiso),
                           "size": 1} for habitantepiso in HabitantePiso.objects.all()]},
            {"name": "Personas con Trastero",
             "children": [{"name": str(personatrastero),
                           "size": 1} for personatrastero in PersonaTrastero.objects.all()]},
            {"name": "Entradas", "size": Entrada.objects.count()},
            {"name": "Salidas", "size": Salida.objects.count()}]
        }
        
    return JsonResponse(data)


def get_entradas_pie_chart_data(request):
    """
    It returns a json containing the number of 'Entrada'
    instances per subclass. It will be used to render the
    pie chart in 'Balance general' section.
    """
    data = {
        "Mantenimiento": int(sum([entrada.importe for entrada in EntradaMantenimiento.objects.all()])),
        "Alquileres": int(sum([entrada.importe for entrada in EntradaAlquiler.objects.all()])),
        "Donativos": int(sum([entrada.importe for entrada in EntradaDonativo.objects.all()])),
        "Pisos": int(sum([entrada.importe for entrada in EntradaPiso.objects.all()])),
        "Alquiler trasteros": int(sum([entrada.importe for entrada in EntradaAlquilerTrastero.objects.all()])),
        "Eventos": int(sum([entrada.importe for entrada in EntradaBeneficiosEvento.objects.all()])),
        "Proyectos Productivos": int(sum([entrada.importe for entrada in EntradaBeneficioProyectoProductivo.objects.all()])) + int(sum([entrada.importe for entrada in EntradaEspacioProyectoProductivo.objects.all()])),
        "Otras entradas": int(sum([entrada.importe for entrada in OtraEntrada.objects.all()])),
    }
    data = [{"label": k, "value": v} for k,v in data.items()]
    return JsonResponse(data, safe=False)


def get_salidas_pie_chart_data(request):
    """
    It returns a json containing the number of 'Salida'
    instances per type. It will be used to render the
    pie chart in 'Balance general' section.
    """
    data = {
        "Alquiler finca": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="alquiler_compra")])),
        "Rehabilitación": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="material_rehabilitacion")])),
        "Butano": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="butano")])),
        "Impuestos": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="impuesto")])),
        "Comisiones": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="comision")])),
        "Internet": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="internet")])),
        "Agua": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="agua")])),
        "Fiare": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="fiare")])),
        "Otras salidas": int(sum([salida.importe for salida in Salida.objects.filter(tipo_salida="otra")]))
    }
    data = [{"label": k, "value": v} for k,v in data.items()]
    
    return JsonResponse(data, safe=False)


def get_balance_general_data(request):
    """
    It returns a json that contains the data to visualize
    linear graph about accounting.
    """
    entradas = [{'fecha': str(entrada.fecha), 'importe': int(entrada.importe)} for entrada in Entrada.objects.filter(fecha__gt=(datetime.now() - timedelta(days=365))).order_by('fecha')]
    salidas = [{'fecha': str(salida.fecha), 'importe': int(salida.importe)} for salida in Salida.objects.filter(fecha__gt=(datetime.now() - timedelta(days=365))).order_by('fecha')]
    data = [
        {
            'label': "Entradas",
            'dates': [entrada['fecha'] for entrada in entradas],
            'prices': [entrada['importe'] for entrada in entradas],
        },
        {
            'label': "Salidas",
            'dates': [salida['fecha'] for salida in salidas],
            'prices': [salida['importe'] for salida in salidas],
        }
    ]

    return JsonResponse(data, safe=False)
        
