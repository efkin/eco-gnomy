from django.contrib import admin
from .models import *

for model in (CuotaMantenimiento,
              CuotaPiso,
              Piso,
              Trastero,
              ProyectoProductivo,
              Habitante,
              Invitada,
              Huesped,
              PersonaTrastero,
              HabitantePiso,
              Metalico,
              Banco,
              PayPal,
              Entrada,
              EntradaMantenimiento,
              EntradaPiso,
              EntradaAlquiler,
              EntradaDonativo,
              EntradaBeneficiosEvento,
              OtraEntrada,
              EntradaEspacioProyectoProductivo,
              EntradaBeneficioProyectoProductivo,
              EntradaAlquilerTrastero,
              EntradaTransferencia,
              Salida,
              Balance):
    admin.site.register(model)


