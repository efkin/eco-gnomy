from django.conf.urls import patterns, url

from . import views


urlpatterns = [

    # GT economia
    ## Cuotas
    url(r'^gt-economia/cuotas/mantenimiento/crear/',
        views.CuotaMantenimientoCreateView.as_view(),
        name="crear-mantenimiento"
        ),
    url(r'^gt-economia/cuotas/mantenimiento/',
        views.CuotaMantenimientoListView.as_view(),
        name="lista-mantenimiento"
        ),
    url(r'^gt-economia/cuotas/piso/crear/',
        views.CuotaPisoCreateView.as_view(),
        name="crear-cuota-piso"
        ),
    url(r'^gt-economia/cuotas/piso/',
        views.CuotaPisoListView.as_view(),
        name="lista-cuotas-piso"
        ),

    ## Espacios
    url(r'^gt-economia/espacios/pisos/crear/',
        views.PisoCreateView.as_view(),
        name="crear-piso"
        ),
    url(r'^gt-economia/espacios/pisos/(?P<pk>\d+)/',
        views.PisoUpdateView.as_view(),
        name="editar-piso"
        ),
    url(r'^gt-economia/espacios/pisos/',
        views.PisoListView.as_view(),
        name="lista-pisos"
        ),
    url(r'^gt-economia/espacios/trasteros/crear/',
        views.TrasteroCreateView.as_view(),
        name="crear-trastero"
        ),
    url(r'^gt-economia/espacios/trasteros/(?P<pk>\d+)/',
        views.TrasteroUpdateView.as_view(),
        name="editar-trastero"
        ),
    url(r'^gt-economia/espacios/trasteros/',
        views.TrasteroListView.as_view(),
        name="lista-trasteros"
        ),

    
    ## Registros
    url(r'^gt-economia/registros/metalico/crear/',
        views.MetalicoCreateView.as_view(),
        name="crear-metalico"
        ),
    url(r'^gt-economia/registros/metalico/',
        views.MetalicoListView.as_view(),
        name="lista-metalicos"
        ),
    url(r'^gt-economia/registros/banco/crear/',
        views.BancoCreateView.as_view(),
        name="crear-banco"
        ),
    url(r'^gt-economia/registros/banco/',
        views.BancoListView.as_view(),
        name="lista-bancos"
        ),
    url(r'^gt-economia/registros/electronico/crear/',
        views.PaypalCreateView.as_view(),
        name="crear-paypal"
        ),
    url(r'^gt-economia/registros/electronico/',
        views.PaypalListView.as_view(),
        name="lista-paypals"
        ),

    ## Proyectos
    url(r'^gt-economia/proyectos/crear/',
        views.ProyectoProductivoCreateView.as_view(),
        name="crear-proyecto"
        ),
    url(r'^gt-economia/proyectos/(?P<pk>\d+)/',
        views.ProyectoProductivoUpdateView.as_view(),
        name="editar-proyecto"
        ),
    url(r'^gt-economia/proyectos/',
        views.ProyectoProductivoListView.as_view(),
        name="lista-proyectos"
        ),

    ## Personas
    url(r'^gt-economia/personas/habitantes/crear/',
        views.HabitanteCreateView.as_view(),
        name="crear-habitante"
        ),
    url(r'^gt-economia/personas/habitantes/(?P<pk>\d+)/',
        views.HabitanteUpdateView.as_view(),
        name="editar-habitante"
        ),
    url(r'^gt-economia/personas/habitantes/',
        views.HabitanteListView.as_view(),
        name="lista-habitantes"
        ),
    url(r'^gt-economia/personas/invitadas/crear/',
        views.InvitadaCreateView.as_view(),
        name="crear-invitada"
        ),
    url(r'^gt-economia/personas/invitadas/(?P<pk>\d+)/',
        views.InvitadaUpdateView.as_view(),
        name="editar-invitada"
        ),
    url(r'^gt-economia/personas/invitadas/',
        views.InvitadaListView.as_view(),
        name="lista-invitadas"
        ),
    url(r'^gt-economia/personas/huespedes/crear/',
        views.HuespedCreateView.as_view(),
        name="crear-huesped"
        ),
    url(r'^gt-economia/personas/huespedes/(?P<pk>\d+)/',
        views.HuespedUpdateView.as_view(),
        name="editar-huesped"
        ),
    url(r'^gt-economia/personas/huespedes/',
        views.HuespedListView.as_view(),
        name="lista-huespedes"
        ),

    ## Pactos
    url(r'^gt-economia/pactos/habitantepiso/crear/',
        views.HabitantePisoCreateView.as_view(),
        name="crear-habitantepiso"
        ),
    url(r'^gt-economia/pactos/habitantepiso/(?P<pk>\d+)/',
        views.HabitantePisoUpdateView.as_view(),
        name="editar-habitantepiso"
        ),
    url(r'^gt-economia/pactos/habitantepiso/',
        views.HabitantePisoListView.as_view(),
        name="lista-habitantepiso"
        ),
    url(r'^gt-economia/pactos/personatrastero/crear/',
        views.PersonaTrasteroCreateView.as_view(),
        name="crear-personatrastero"
        ),
    url(r'^gt-economia/pactos/personatrastero/(?P<pk>\d+)/',
        views.PersonaTrasteroUpdateView.as_view(),
        name="editar-personatrastero"
        ),
    url(r'^gt-economia/pactos/personatrastero/',
        views.PersonaTrasteroListView.as_view(),
        name="lista-personatrastero"
        ),

    ## Movimientos
    url(r'^gt-economia/movimientos/salidas/crear/',
        views.insertar_salidas,
        name="crear-salida"
        ),
    url(r'^gt-economia/movimientos/entradas/otras/crear/',
        views.insertar_entradas_otras,
        name="crear-otras-entradas"
        ),
    url(r'^gt-economia/movimientos/entradas/alquileres/crear/',
        views.insertar_entradas_alquileres,
        name="crear-alquileres-entradas"
        ),
    url(r'^gt-economia/movimientos/entradas/proyectos/crear/',
        views.insertar_entradas_proyectos,
        name="crear-proyectos-entradas"
        ),

    ## Balances
    url(r'^gt-economia/balances/crear/',
        views.BalanceCreateView.as_view(),
        name="crear-balance"
        ),
    url(r'^gt-economia/balances/(?P<pk>\d+)/',
        views.BalanceDetailView.as_view(),
        name="ver-balance"
        ),
    url(r'^gt-economia/balances/',
        views.BalanceListView.as_view(),
        name="lista-balances"
        ),

    ## Dashboard
    url(r'^gt-economia/', views.gt_dashboard, name='gt-dashboard'),

    # Public dashboard
    ## API
    url(r'^api/cloud-data/', views.get_dashboard_data, name='api-cloud-data'),
    url(r'^api/entradas-pie/', views.get_entradas_pie_chart_data, name='api-entradas-pie'),
    url(r'^api/salidas-pie/', views.get_salidas_pie_chart_data, name='api-salidas-pie'),
    url(r'^api/balance-line-graph/', views.get_balance_general_data, name='api-balance-line-graph'),

    ## Situaciones personales
    url(r'^situaciones-personales/(?P<habitante_id>\d+)/', views.situaciones_personales, name='situaciones-personales'),
    url(r'^situaciones-personales/', views.situaciones_personales, name='situaciones-personales'),

    ## Balance general
    url(r'^balance-general/', views.balance_general, name='balance-general'),
    
    ## Index
    url(r'^$', views.index, name='index'),

]

