from django import template


register = template.Library()


@register.inclusion_tag("tags/form_field.html")
def form_field(field, hide_label=False, hide_help=False):
    """
    Tag for rendering uniformly form fields.
    """
    return {
        'field': field,
        'hide_label': hide_label,
        'hide_help': hide_help,
        }


@register.inclusion_tag("tags/non_field_errors.html")
def non_field_errors(form):
    """
    Tag for rendering uniformly non form field errors
    """
    return {
        'form': form,
        }
