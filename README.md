# Eco-gnomy
A Django project to manage Calafou's housing cooperative written in python3.

## How to set up a development enviroment?
Read please `docs/entorno-de-desarrollo.md`

## Front-end hints

* Visit `localhost:8000/foundation/` after the dev server is running.
* For icons visit `localhost:8000/foundation/icons/` after the dev server is running.
* Checkout `ecognome/druids/static/druids/` for static files.
* Base template is under `ecognome/ecognome/templates/base.html`.
* Base `druids` app template is under `ecognome/ecognome/templates/druids.html`.

